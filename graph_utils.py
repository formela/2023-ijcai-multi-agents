from collections import Counter
import torch
import networkx
import numpy as np
from itertools import product
from termcolor import cprint
from graph_collections import graphs_dict
from graph_modifiers import graph_modifier_from_string
from itertools import combinations


class Graph(object):
    def __init__(self, loader, loader_params, modifiers=None, verbose=False, name=None, num_of_agents=1, agent_resilience=0, global_memory=1):
        self.verbose = verbose
        self.name = name
        self.num_of_agents = num_of_agents
        self.global_memory = global_memory

        self.graph = load_graph_with_modifiers(loader, loader_params, modifiers, verbose)
        self.graph = self.graph.to_directed()

        self.targets, self.non_targets = [], []
        for (node, is_target) in sorted(self.graph.nodes.data('target')):
            (self.non_targets, self.targets)[is_target].append(node)
        self.targets_n = len(self.targets)

        self.nodes = self.targets + self.non_targets  # external evaluators require targets first
        self.nodes_n = len(self.nodes)

        # collecting node features
        target_features = ['value', 'memory', 'attack_len', 'blindness', 'delimiters', 'rewards']
        self.target_features = get_node_features(self.graph, self.targets, target_features)
        non_target_features = ['memory']
        self.non_target_features = get_node_features(self.graph, self.non_targets, non_target_features)

        # integer encoding for external evaluators
        self.node_map = {node: index for index, node in enumerate(self.nodes)}
        self.edges = [(self.node_map[a], self.node_map[b], att['len']) for a, b, att in self.graph.edges.data()]

        # graph augmentation with memory elements
        self.aug_nodes, aug_nodes_att = [], []
        for node in self.nodes:
            self.aug_nodes += [(node, mem) for mem in range(self.graph.nodes[node]['memory'])]
            # aug_nodes_att += [self.graph.nodes[node]] * self.graph.nodes[node]['memory']
        self.aug_node_map = {node: index for index, node in enumerate(self.aug_nodes)}
        self.aug_nodes_n = len(self.aug_nodes)

        self.aug_graph = networkx.DiGraph()
        self.aug_graph.add_nodes_from(self.aug_nodes)  # the nodes do not contain their attributes

        for source, destination, att in self.graph.edges.data():
            source_memory = self.graph.nodes[source]['memory']
            destination_memory = self.graph.nodes[destination]['memory']
            mem_product = product(range(source_memory), range(destination_memory))
            aug_edges = [((source, ms), (destination, mt), att) for ms, mt in mem_product]
            self.aug_graph.add_edges_from(aug_edges)

        # edge_len matrix indexed by aug_nodes
        times_before_ma = networkx.to_numpy_array(self.aug_graph, nodelist=self.aug_nodes, weight='len')
        self.times_before_ma = torch.from_numpy(times_before_ma)

        # binary matrix; edge encodings of aug_edges
        self.mask_before_ma = self.times_before_ma > 0

        # product for multi-agent models - nodes
        ma_nodes_tuple = list(product(self.aug_nodes, repeat=self.num_of_agents))
        self.ma_nodes = list(product(ma_nodes_tuple, range(self.global_memory)))
        self.ma_nodes_n = len(self.ma_nodes)

        self.ma_graph = networkx.DiGraph()
        self.ma_graph.add_nodes_from(self.ma_nodes)  # the nodes do not contain their attributes

        # product for multi-agent models - edges
        mem_product = list(product(range(self.global_memory), range(self.global_memory)))
        for source_tuple in ma_nodes_tuple:
            list_of_destinations = [[x for x in self.aug_graph.successors(source)] for source in source_tuple]
            for destination_tuple in product(*list_of_destinations):
                ma_edges = [((source_tuple, ms), (destination_tuple, mt), {'len': 1}) for ms, mt in mem_product]
                self.ma_graph.add_edges_from(ma_edges)

        # rewrite aug_graph by ma_graph
        self.aug_graph = self.ma_graph
        self.aug_nodes = self.ma_nodes
        self.aug_node_map = {node: index for index, node in enumerate(self.aug_nodes)}
        self.aug_nodes_n = self.ma_nodes_n

        # edge_len matrix indexed by aug_nodes
        times = networkx.to_numpy_array(self.aug_graph, nodelist=self.aug_nodes, weight='len')
        self.times = torch.from_numpy(times)

        # binary matrix; edge encodings of aug_edges
        self.mask = self.times > 0

        # bool matrix mask Targets x AugNodes
        # row i encodes by True all aug_nodes corresponding to target[i]
        target_mask = []
        self.aug_targets = [] if not agent_resilience else list(product(self.targets, combinations(range(self.num_of_agents), agent_resilience)))
        self.aug_target_map = torch.LongTensor([self.node_map[t] for t, _ in self.aug_targets])

        for target in self.targets:
            target_mask.append([target in [x for x, _ in aug_node] for aug_node, _ in self.aug_nodes])
        for target, dis_agents in self.aug_targets:
            target_mask.append([target in [x for i, (x, _) in enumerate(aug_node) if i not in dis_agents] for aug_node, _ in self.aug_nodes])
        self.target_mask = torch.tensor(target_mask, dtype=torch.bool)

        # array; for an aug_node, the longest incoming edge length
        self.longest = self.times.max(dim=0).values

        self.costs = torch.tensor(self.target_features['value'])
        self.aug_costs = torch.concat([self.costs, self.costs[self.aug_target_map]])

        rewards = self.target_features['rewards']
        delimiters = self.target_features['delimiters']
        nan_rewards = [type(x) is float and np.isnan(x) for x in rewards]
        if not(all(nan_rewards)):
            # there are some rewards
            for rew, deli in zip(rewards, delimiters):
                if isinstance(rew, list):
                    rew += [0] * (2 + len(deli) - len(rew))
            self.reward_decay = torch.tensor(sum([reward[-1] for reward in self.target_features['rewards']]))

        if verbose:
            self.print_graph_stats()

    def get_patrolling_init_data(self):
        data = {'memory': self.target_features['memory'] + self.non_target_features['memory'],
                'attack_len': self.target_features['attack_len'],
                'value': self.target_features['value'],
                'blindness': self.target_features['blindness'],
                'edges': self.edges}
        return data

    def get_maintenance_init_data(self):
        data = {'memory': self.target_features['memory'] + self.non_target_features['memory'],
                'delimiters': self.target_features['delimiters'],
                'rewards': self.target_features['rewards'],
                'edges': self.edges}
        return data

    def __repr__(self):
        return self.name or self.graph.name or self.__class__.__name__

    def print_strategy(self, strategy, style='label', precision=3, print_len=False, delay=None):
        cprint('Strategy:', 'blue')
        print_delay = delay is not None
        if style == 'matrix':
            with np.printoptions(precision=precision, suppress=True, threshold=np.inf, linewidth=np.inf):
                print(' ', np.array2string(strategy, prefix='  '))
                if print_len:
                    cprint('Length:', 'blue')
                    print(' ', np.array2string(self.times.numpy(), prefix='  '))
                if print_delay:
                    cprint('Delay:', 'blue')
                    print(' ', np.array2string(delay, prefix='  '))
        else:
            print('    src--dest :\tprob' + ' ' * (precision - 1) + '\t' + 'length' * print_len
                  + ' +' * print_len*print_delay + ' delay' * print_delay)
            for (source, ms), (destination, mt), length in self.aug_graph.edges(data='len'):
                source_index = self.aug_node_map[(source, ms)]
                destination_index = self.aug_node_map[(destination, mt)]
                prob = strategy[source_index, destination_index]
                edge_delay = delay[source_index, destination_index] if print_delay else 0
                if prob > 0:
                    if style == 'label':
                        edge_str = f'{source} ({ms})--{destination} ({mt})'
                    elif style == 'graphviz':
                        edge_str = f'"{source}M{ms}"->"{destination}M{mt}"'
                    elif style == 'index':
                        edge_str = f'{source_index:5.0f}--{destination_index:3.0f}  '
                    else:
                        raise AttributeError(f'unknown printing style "{style}"')

                    if style != 'graphviz':
                        print(f'  {edge_str}:\t{prob:.{precision}f}\t'
                              + f' {length:.{precision}f}' * print_len
                              + ' +' * print_len * print_delay
                              + f' {edge_delay:.{precision}f}' * print_delay)
                    else:
                        print(f'  {edge_str}[label={prob:.{precision}f}]')

    def print_graph_stats(self):
        cprint(f'Nodes: ({self.graph.number_of_nodes()})', 'blue')
        for node in self.graph.nodes.data():
            print('  ', node)
        cprint(f'Edges: ({self.graph.number_of_edges()})', 'blue')
        for edge in self.graph.edges.data():
            print('  ', edge)
        cprint(f'Incidence matrix', 'blue')
        with np.printoptions(precision=1, suppress=True, threshold=np.inf, linewidth=np.inf):
            print(np.array2string(networkx.to_numpy_matrix(self.graph, nodelist=self.nodes, weight='len')))
        lengths = [l for _, _, l in self.graph.edges.data('len')]
        cprint('Graph statistics:', 'blue')
        print(f'  edge length: min={min(lengths)}\tmax={max(lengths)}\tavg={np.mean(lengths):.2f}')
        print(f'  degrees:')
        if self.graph.is_directed():
            out_deg = Counter(d for n, d in self.graph.out_degree())
            in_deg = Counter(d for n, d in self.graph.in_degree())
            for (out_degree, out_count), (in_degree, in_count) in zip(out_deg.most_common(), in_deg.most_common()):
                print(f'   in={in_degree:01d}:\t{in_count}x\tout={out_degree:01d}:\t{out_count}x')
        cprint('Target features', 'blue')
        for key, features in self.target_features.items():
            cprint(f'  {key}:', 'blue')
            print('  ', features)
        cprint('Number of agents:', 'blue', end=' ')
        print(self.num_of_agents)
        cprint('Global memory:', 'blue', end=' ')
        print(self.global_memory)

    def get_figure(self):
        if self.graph.name.__contains__('square_subgraph'):
            pos = {node: node for node in self.graph.nodes()}
        # elif self.graph.name.__contains__('office_building'):
        #     pos = networkx.planar_layout(self.graph)
        else:
            pos = networkx.spring_layout(self.graph)
        networkx.draw(self.graph, with_labels=True, pos=pos)
        labels = {tuple(edge): label for *edge, label in self.graph.edges.data('len')}
        networkx.draw_networkx_edge_labels(self.graph, pos=pos, edge_labels=labels, label_pos=0.3)
        # plt.savefig(os.path.join(self.results_dir, 'graph.pdf'))
        # todo: return fig binary
        # https://stackoverflow.com/questions/31492525/converting-matplotlib-png-to-base64-for-viewing-in-html-template
        # https://stackoverflow.com/questions/57222519/how-to-get-a-figure-binary-object-by-matplotlib
        return 0


def graph_loader_from_string(loader):
    dct = {'identity': lambda graph: graph,
           **graphs_dict}
    return dct[loader]


def load_graph_with_modifiers(loader, loader_params, modifiers, verbose=False):
    graph = graph_loader_from_string(loader)(**loader_params)
    modifiers = modifiers or {}
    if verbose:
        cprint('Graph preprocessing:', 'green')
    for modifier, modifier_params in modifiers.items():
        # modifier = list(modifier_dict)[0]
        # modifier_params = modifier_dict[modifier]
        if verbose:
            cprint(f' {modifier}', 'green')
        graph = graph_modifier_from_string(modifier)(graph, **modifier_params, verbose=verbose)
    if verbose:
        print('done')
    return graph


def get_node_features(graph, nodes, features):
    return {key: [graph.nodes[n].get(key, float('nan')) for n in nodes] for key in features}
