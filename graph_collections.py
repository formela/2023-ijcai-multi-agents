import networkx
from graph_modifiers import delete_k_rnd_edges


def hardcoded(nodes, targets, edges, settings=None):
    nodes = nodes or {}
    targets = targets or {}
    edges = edges or {}
    settings = settings or {}
    node_att = settings.get('node_att', {}).copy()
    node_att.update(target=False)
    target_att = settings.get('target_att', {}).copy()
    target_att.update(target=True)
    edge_att = settings.get('edge_att', {}).copy()

    nodes = [(node, {**node_att, **(att or {})}) for node, att in nodes.items()]
    targets = [(target, {**target_att, **(att or {})}) for target, att in targets.items()]
    edges = [(*edge.get('nodes'), {**edge_att, **edge}) for edge in edges]

    directed = settings.get('directed', True)
    graph = networkx.DiGraph() if directed else networkx.Graph()
    graph.add_nodes_from(targets + nodes)
    graph.add_edges_from(edges)
    graph.name = 'hardcoded'

    return graph


def path_graph(num_nodes, node_att=None):
    node_attributes = dict(value=100, attack_len=num_nodes, blindness=0.0, memory=1, target=True)
    node_attributes.update(node_att or {})
    graph = networkx.path_graph(num_nodes)
    for attr in node_attributes.keys():
        networkx.set_node_attributes(graph, values=node_attributes[attr], name=attr)
    networkx.set_edge_attributes(graph, values=1, name='len')
    graph.name = f'{num_nodes}_path'
    return graph


def grid_subgraph(width, height, removed_edges=None, num_removed_edges=None, seed=0, node_att=None, targets=None):
    if removed_edges is None and num_removed_edges is None:
        raise ValueError('Either removed_edges or num_removed_edges must be given')
    node_attributes = dict(value=1, attack_len=1, blindness=0.0, memory=1, target=targets is None)
    node_attributes.update(node_att or {})
    graph = networkx.grid_2d_graph(width, height)
    if num_removed_edges is not None:
        graph = delete_k_rnd_edges(graph, num_removed_edges, seed)
        print(graph)
    else:
        removed_edges = [(tuple(x), tuple(y)) for x, y in removed_edges] + [(tuple(y), tuple(x)) for x, y in removed_edges]
        graph.remove_edges_from(removed_edges)
    for attr in node_attributes.keys():
        networkx.set_node_attributes(graph, values=node_attributes[attr], name=attr)
    if targets:
        for x, y in targets:
            graph.nodes[(x, y)]['target'] = True
    networkx.set_edge_attributes(graph, values=1, name='len')
    graph.name = f'{width}x{height}_random_grid'
    return graph


graphs_dict = {
    'hardcoded': hardcoded,
    'grid_subgraph': grid_subgraph,
    'path_graph': path_graph,
}
