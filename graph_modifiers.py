import random
import networkx


def delete_k_rnd_edges(graph, k, seed=None, verbose=False):
    """Generates and returns a random graph as a modification of 'source_graph'.
       'k' randomly chosen edges are deleted such that the final graph is still strongly connected.
        @param verbose:
        @param graph:          the original graph
        @param k:              the number of edges to be deleted
        @param seed:           the seed for pseudorandom functions
    """
    graph = networkx.DiGraph(graph)
    edge_list = list(graph.edges(data=True))

    if seed is not None:
        random.seed(seed)

    while k > 0:
        chosen_edge = random.choice(edge_list)
        graph.remove_edge(chosen_edge[0], chosen_edge[1])
        if networkx.is_strongly_connected(graph):
            k -= 1
            edge_list.remove(chosen_edge)
            if verbose:
                print(f'  Removing edge: {chosen_edge[0]}--{chosen_edge[1]}')
        else:
            graph.add_edges_from([chosen_edge])
    return graph



def graph_modifier_from_string(modifier):
    dct = {
           'delete_k_rnd_edges': delete_k_rnd_edges,
    }
    return dct[modifier]
