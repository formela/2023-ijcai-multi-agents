from graph_utils import Graph
from reporting import ExperimentReporting, RunReporting
from model import objectives_dict, Model
from trainer import StrategyTrainer
from utils import set_seed, parse_params


def train_one_epoch(epoch, seed, graph, model_params, trainer_params, reporting_params, verbose):
    set_seed(seed, epoch)

    model = Model(graph, **model_params)

    report = RunReporting(epoch, **reporting_params)

    StrategyTrainer(model, report, **trainer_params, verbose=verbose).train()

    return report.dump()


@parse_params()
def main(seed, verbosity, reporting_params, epochs, graph_params, model_params, trainer_params):
    settings = locals()
    mode = objectives_dict[model_params['objective']].mode
    report = ExperimentReporting(**reporting_params, settings=settings, mode=mode, verbose=(verbosity >= 1))

    graph = Graph(**graph_params, verbose=(verbosity >= 2))
    report.add_graph(graph)

    for epoch in range(epochs):
        metrics = train_one_epoch(epoch=epoch,
                                  seed=seed,
                                  graph=graph,
                                  model_params=model_params,
                                  trainer_params=trainer_params,
                                  reporting_params=report.params,
                                  verbose=(verbosity >= 3))
        report.update(metrics)

    report.dump()


if __name__ == '__main__':
    main()
