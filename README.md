# Synthesizing Resilient Strategies for Infinite-Horizon Objectives in Multi-Agent Systems (IJCAI 2023)

Consider the problem of synthesizing resilient and stochastically stable strategies for systems of cooperating agents striving to minimize the expected time between consecutive visits to selected locations in a known environment. A strategy profile is resilient if it retains its functionality even if some of the agents fail, and stochastically stable if the visiting time variance is small. We design a novel specification language for objectives involving resilience and stochastic stability, and we show how to efficiently compute strategy profiles (for both autonomous and coordinated agents) optimizing these objectives. 

Here, there is the implementation of the algorithm computing strategy profiles. Our experiments show that our strategy synthesis algorithm can construct highly non-trivial and efficient strategy profiles for environments with general topology.

To appear in [IJCAI 2023](https://ijcai-23.org/).


David Klaška, Antonín Kučera, Martin Kurečka, Vít Musil, Petr Novotný, Vojtěch Řehák. Synthesizing Resilient Strategies for Infinite-Horizon Objectives in Multi-Agent Systems. In Proceedings of the Thirty-Second International Joint Conference on Artificial Intelligence Main Track. Pages 171-179. https://doi.org/10.24963/ijcai.2023/20


The arXiv version is available [here](https://arxiv.org/abs/2305.10070).

Up-to-date version of Regstar is at [gitlab.fi.muni.cz/formela/regstar](https://gitlab.fi.muni.cz/formela/regstar/).


## Preparation

Create virtual environment and install all packages:

1. Make sure you have `python 3` and `pipenv` installed. For the up-to-date version, run
```shell
python3 -m pip install --upgrade pip
python3 -m pip install pipenv
cd /the-main-directory-of-implementation/
python3 -m pipenv --rm
```
2. Run `python3 -m pipenv install` (at your own risk with `--skip-lock`)
3. Run the environment `python3 -m pipenv shell`
4. Install ccp_extensions by `cd cpp_extensions; python setup.py build install; cd ..`


## Optimization

Run the optimization (in the above installed pipenv shell) by
```shell
python3 train.py path/to/your/settings.yml
```
where `path/to/your/settings.yml` is the path to your configuration file.

Alternatively, run
```shell
python3 train_parallel.py path/to/your/settings.yml
```
which runs more optimizations in parallel.

For more details, see `setting_template.yml` or the examples in `experiments/.../*.yml`.

## Results

The results can be saved locally to `disk`.

### Disk

Enabled by setting
```yaml
reporting_params:
  database: disk
  database_params:
    results_dir: path/to/a/directory # usually 'results/<experiment_name>/<date>/<time>'
    # more params ...
```
The results, visualizations and logs are stored provided `results_dir`, including

* `stats_epoch.csv` Detailed statistics for all epochs
* `stats_final.csv` Statistics summary over all epochs
* `stats_full.csv` Detailed statistics for all steps of all epochs
* `times_full.csv` Time statistics for all steps of all epochs

and optionally also:

* `check_points` Directory with exports of all torch parameter values
* `strategies` Directory with optimized strategies exported as numpy matrices
* `plots` Directory with plots:
  * `avg_times.pdf` Plot of average runtimes ("forward", "backward", and "other" times)
  * `training_progress.pdf` Plots of strategy values during optimization

