import os
import socket
import git
from time import process_time
from writers import run_writers_dct, experiment_writers_dct
from copy import deepcopy

mode_dct = {
    'min': -1,
    'max': 1
}


class ExperimentReporting(object):
    def __init__(self, database, database_params, settings, mode, tag=None, description=None, verbose=False):
        self.mode = mode
        self.verbose = verbose
        self.writer = experiment_writers_dct[database](**database_params)
        report_settings = deepcopy(settings)
        try:
            report_settings['reporting_params']['database_params']['client_params'].pop('password', None)
        except KeyError:
            pass
        self.writer.add('settings', report_settings)
        self.writer.add('tag', tag)
        self.writer.add('description', description)
        self.writer.add('hostname', socket.gethostname())
        self.writer.add('script', os.path.basename(__file__))
        try:
            commit = git.Repo(os.getcwd()).head.reference.commit.hexsha[:7]
        except:
            commit = None
        self.writer.add('commit', commit)

        self.params = {'database': database,
                       'database_params': self.writer.params,
                       'mode': self.mode,
                       'verbose': verbose,
                       'tag': tag,
                       'experiment_id': self.writer.id}

        self.start_time = process_time()

    def update(self, run):
        self.writer.add_run(run)

    def dump(self):
        aggregations = [
            ('metrics_stats.best_val', 'metrics.best_val', self.mode),
            ('metrics_stats.best_val', 'metrics.avg_best_val', 'avg'),
            ('metrics_stats.avg_val', 'metrics.avg_val', 'avg'),
            # todo: best_val_at_epoch
            # ('metrics.epoch', 'metrics.best_val_at_epoch', 'argmax/argmin'),  #not defined!!
            ('times_stats.total', 'times.avg_epoch', 'avg'),
            ('times_stats.avg_sum', 'times.avg_step', 'avg'),
        ]
        aggregations += [(f'times_stats.avg_{x}', f'step_times.avg_{x}', 'avg') for x in
                         ['forward', 'backward', 'optimizer', 'eval']]
        self.writer.aggregate_stats(aggregations)

        self.writer.add('times.total', process_time() - self.start_time)

        if self.verbose:
            self.print_stats(self.writer.view())
        return self.writer.close()

    def add_graph(self, graph):
        # self.writer.add_binary('graph.graph', graph.graph_before_ma)  # hack for drawing graphs with multiple_agents
        # self.writer.add_binary('graph.graph', graph.graph)
        self.writer.add_binary('graph.aug_nodes', graph.aug_nodes)

    def print_stats(self, data):
        msg = '-------------'
        msg += f"\nbest val\t= {data['metrics']['best_val']:.3f}"
        msg += f"\navg epoch time\t= {data['times']['avg_epoch']:.3f}"
        msg += f"\navg step time\t= {data['times']['avg_step']:.3f}"
        msg += f"\ntotal time\t= {data['times']['total']:.1f}"
        print(msg)

        # todo: print the strategy for best_val
        # self.graph.print_strategy(strategy= ...)


class RunReporting(object):
    def __init__(self, epoch, database, database_params, tag, experiment_id, mode, verbose):
        epoch += 1
        self.mode = mode
        self.verbose = verbose
        self.writer = run_writers_dct[database](epoch, **database_params)
        self.writer.add('tag', tag)
        self.writer.add('experiment', experiment_id)
        self.writer.add('epoch', epoch)

        self.best_val = - mode_dct[self.mode] * float('inf')
        self.best_at = 0
        self.extras = set()
        self.start_time = process_time()

    def update(self, step_num, val, val_train, loss,
               strategy, delay, state_dict,
               f_time, b_time, o_time, e_time,
               extras=None):

        self.writer.add_scalar('metrics.val', val)
        self.writer.add_scalar('metrics.val_train', val_train)
        self.writer.add_scalar('metrics.loss', loss)
        self.writer.add_scalar('times.forward', f_time)
        self.writer.add_scalar('times.backward', b_time)
        self.writer.add_scalar('times.optimizer', o_time)
        self.writer.add_scalar('times.eval', e_time)
        self.writer.add_scalar('times.sum', f_time + b_time + o_time + e_time)

        extras = extras or {}
        for key, v in extras.items():
            self.extras.add(key)
            self.writer.add_scalar(f'metrics.{key}', v)

        if mode_dct[self.mode] * val > mode_dct[self.mode] * self.best_val:
            self.best_at = step_num
            self.best_val = val
            self.writer.add('metrics_stats.best_val', val)
            self.writer.add('metrics_stats.best_val_at', step_num)
            self.writer.add_binary('strategy', strategy)
            if delay is not None:
                self.writer.add_binary('delay', delay)
            self.writer.add_binary('state_dict', state_dict)

    def dump(self):
        self.writer.add('times_stats.total', process_time() - self.start_time)
        aggregations = [
            # ('metrics.val', 'metrics_stats.best_val', self.mode),  # not needed, already in metrics.best_val
            ('metrics.val', 'metrics_stats.end_val', 'last'),
            ('metrics.val', 'metrics_stats.avg_val', 'avg'),
        ]
        aggregations += [(f'metrics.{key}', f'metrics_stats.{key}', 'arrayElemAt', self.best_at - 1) for key in self.extras]
        aggregations += [(f'times.{x}', f'times_stats.avg_{x}', 'avg') for x in
                         ['forward', 'backward', 'optimizer', 'eval', 'sum']]
        self.writer.aggregate_stats(aggregations)

        if self.verbose:
            self.print_run(self.writer.view())
        # returns either id for 'mongo' or data for 'disk'
        return self.writer.close()

    def print_run(self, data):
        metrics = data['metrics_stats']
        times = data['times_stats']
        msg = f"epoch {data['epoch']}:"
        msg += f"\tend val={metrics['end_val']:.2f}"
        msg += f"\tbest val={metrics['best_val']:.2f} at {metrics['best_val_at']}"
        for key in self.extras:
            msg += f"\t{key}={metrics[key]:.1f}"
        msg += f"\ttime={times['total']:.2f}"
        msg += f"\tavg step time={times['avg_sum']:.3f}"
        print(msg)
