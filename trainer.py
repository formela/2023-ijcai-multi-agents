import math
from copy import deepcopy

import numpy as np
import torch
from torch.optim import Adam, SGD
from torch.optim.lr_scheduler import StepLR, MultiStepLR, MultiplicativeLR
from time import process_time
from termcolor import cprint


class StrategyTrainer(object):
    def __init__(self, model, report, steps, eval_every=1,
                 optimizer=None, optimizer_params=None,
                 noise=None, noise_params=None,
                 scheduler=None, scheduler_params=None,
                 post_hooks=None,
                 verbose=False, strategy_style='matrix'):
        noise_params = noise_params or {}
        scheduler_params = scheduler_params or {}
        optimizer_params = optimizer_params or {}
        self.post_hooks = post_hooks or {}
        self.verbose = verbose
        self.strategy_style = strategy_style
        self.report = report
        # self.report.steps = steps
        self.step_num = 0
        self.val = 0.0
        self.strategy = None
        self.delay = None
        self.state_dict = None
        self.val_train = None
        self.loss = None
        self.times = None
        self.extras = None
        self.eval_extras = {}
        self.steps = steps
        self.eval_every = eval_every
        self.model = model
        if optimizer is not None:
            params = []
            for child in self.model.children():
                if list(child.parameters()):
                    param_dict = {'params': child.parameters()}
                    if hasattr(child, 'lr') and child.lr is not None:
                        param_dict['lr'] = child.lr
                    params.append(param_dict)
            self.optimizer = optimizer_from_string(optimizer)(params=params, **optimizer_params)
        else:
            raise AttributeError('Optimizer not set')
        self.scheduler = scheduler
        if scheduler is not None:
            self.scheduler = scheduler_from_string(scheduler)(self.optimizer, **scheduler_params)
        self.noise_decay = None
        if noise is not None:
            self.noise_decay = noise_from_string(noise)(**noise_params)

    def noise(self):
        if self.noise_decay is not None:
            rand_noise = torch.clamp(torch.randn_like(self.model.strategy.params.grad), -3.0, 3.0)
            step_noise = rand_noise * self.noise_decay.apply(self.val_train, self.step_num)
            self.model.strategy.params.grad += step_noise

    def eval(self):
        if self.step_num % self.eval_every == 0 or self.step_num == 1:
            self.model.eval()
            with torch.no_grad():
                self.val, self.strategy, self.eval_extras = self.model()
                self.state_dict = deepcopy(self.model.state_dict())
                self.print_eval()

    def step(self):
        start_time = process_time()
        self.model.train()
        self.step_num += 1
        self.print_start_step()
        self.optimizer.zero_grad()
        self.model.schedule(self.steps, self.step_num)
        self.loss, self.val_train, self.extras = self.model()
        self.print_step()
        forward_time = process_time()
        self.loss.backward()
        self.print_grad()
        backward_time = process_time()
        self.noise()
        self.optimizer.step()
        if self.scheduler is not None:
            self.scheduler.step()
        optimizer_time = process_time()
        # !! HACK for periodic_maintenance
        self.eval()
        # end HACK
        eval_time = process_time()
        self.times = dict(
            f_time=forward_time - start_time,
            b_time=backward_time - forward_time,
            o_time=optimizer_time - backward_time,
            e_time=eval_time - optimizer_time)
        self.log()

    def train(self):
        while self.step_num < self.steps:
            self.step()
            if self.model.objective.early_stop(self.val):
                break

    def log(self):
        self.extras = self.extras or {}
        self.eval_extras = self.eval_extras or {}
        self.report.update(
            step_num=self.step_num,
            val=self.val,
            val_train=self.val_train,
            loss=self.loss.item(),
            strategy=self.strategy,
            delay=self.delay,
            state_dict=self.state_dict,
            extras={**self.extras, **self.eval_extras},
            **self.times)

    def print_eval(self):
        if self.verbose:
            cprint(f'step={self.step_num} (eval)\t val={self.val:.2f}', 'cyan')
            self.model.graph.print_strategy(self.strategy, delay=self.model.delay, style=self.strategy_style,
                                            precision=3)

    def print_start_step(self):
        if self.verbose:
            cprint(f'step={self.step_num} (train) started', 'green')

    def print_step(self):
        if self.verbose:
            cprint(f'step={self.step_num} (train)\t val={self.val_train:.2f}\t loss={self.loss:.2f}', 'green')

    def print_grad(self):
        if self.verbose:
            with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
                cprint(f'Strategy grad:', 'blue')
                print(' ', np.array2string(self.model.strategy.params.grad.numpy(), prefix='  '))
                if self.model.delay is not None:
                    cprint(f'Delay grad:', 'blue')
                    print(' ', np.array2string(self.model.delay.params.grad.numpy(), prefix='  '))


class ExpDecay(object):
    def __init__(self, tau=1):
        self.tau = tau

    def apply(self, step):
        return math.exp(-self.tau * step)


class PolynomialDecay(object):
    def __init__(self, ord=1):
        self.ord = ord

    def apply(self, step):
        return 1.0 / (step ** self.ord)


class ConstNoise(object):
    def __init__(self, amplitude):
        self.amplitude = amplitude

    def apply(self, val, step):
        return self.amplitude


class BasicNoise(object):
    def __init__(self, amplitude=1.0, decay='Polynomial', decay_params=None):
        self.amplitude = amplitude
        decay_params = decay_params or {}
        self.decay = decay_from_string(decay)(**decay_params)

    def apply(self, val, step):
        return self.amplitude * self.decay.apply(step)


class IncreaseNoiseOnPlateau(BasicNoise):
    def __init__(self, amplitude, threshold, averaging, cooldown, decay, decay_params):
        super().__init__(amplitude, decay, decay_params)
        self.averaging = averaging
        self.threshold = threshold
        self.cooldown = cooldown
        self.vals = []
        self.cooldown_counter = 0

    def apply(self, val, step):
        self.vals.append(val)
        al = self.averaging

        if step >= 2 * al:
            curr_sum = sum(self.vals[-al:])
            prev_sum = sum(self.vals[-2 * al:-al])
            if 1 < curr_sum / prev_sum < 1 + self.threshold and self.cooldown_counter >= 0:
                self.amplitude += 1.0
                self.cooldown_counter = -self.cooldown
            else:
                self.cooldown_counter += 1

        return self.amplitude * self.decay.apply(step)


def optimizer_from_string(optimizer_name):
    dct = {'Adam': Adam,
           'SGD': SGD}
    return dct[optimizer_name]


def scheduler_from_string(scheduler_name):
    dct = {'StepLR': StepLR,
           'MultiStepLR': MultiStepLR,
           'MultiplicativeLR': MultiplicativeLR}
    return dct[scheduler_name]


def noise_from_string(noise_decay):
    dct = {'Basic': BasicNoise,
           'Const': ConstNoise,
           'IncreaseOnPlateau': IncreaseNoiseOnPlateau}
    return dct[noise_decay]


def decay_from_string(noise_decay):
    dct = {'Exp': ExpDecay,
           'Polynomial': PolynomialDecay}
    return dct[noise_decay]

