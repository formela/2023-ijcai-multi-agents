import argparse
import ast
import collections
import functools
import os
import random
from datetime import datetime
import numpy
import torch
import yaml
from termcolor import cprint


def parse_params(use_results_dir=False, use_verbosity=True, save=True, post_unpack_hooks=None):
    def decorator(main_func):
        @functools.wraps(main_func)
        def wrapper(*args, **kwargs):
            parser = argparse.ArgumentParser()
            parser.add_argument('settings', help='path to a YAML settings file')
            parser.add_argument('updates', nargs='*', type=str,
                                help='optional parameters updates of the form \'param=value\'')
            args = parser.parse_args()
            post_hooks = [functools.partial(add_cmdline_params, args.updates)]
            if use_verbosity:
                post_hooks.append(verbosity_check)
            if use_results_dir:
                post_hooks.append(create_results_dir)
            if post_unpack_hooks:
                post_hooks += post_unpack_hooks
            if save:
                post_hooks.append(save_settings)
            post_hooks.append(print_settings)

            if is_settings_file(args.settings):
                params = load_settings(args.settings)
            elif is_parseable_dict(args.settings):
                settings = yaml.dump(ast.literal_eval(args.settings))
                params = load_settings(settings)
            else:
                raise ValueError(f'Failed to parse {args.settings}')

            for hook in post_hooks:
                hook(params)

            return main_func(**params)

        return wrapper

    return decorator


def create_results_dir(params):
    if 'results_dir' in params:
        params['results_dir'] = params['results_dir'].replace('{timestamp}', datetime.now().strftime('%y%h%d/%H-%M-%S'))
        params['results_dir'] = os.path.join(os.getcwd(), params['results_dir'])
        os.makedirs(params['results_dir'], exist_ok=True)
    else:
        raise AttributeError(f"'results_dir' not set")


def save_settings(params):
    if 'results_dir' in params:
        filename = os.path.join(params['results_dir'], 'settings.yml')
        with open(filename, 'w') as stream:
            stream.write(yaml.dump(params, sort_keys=False))


def print_settings(params):
    if params.get('verbosity', 0) >= 2:
        cprint('Running with settings:', 'blue')
        print(yaml.dump(params, sort_keys=False))


def verbosity_check(params):
    if 'verbosity' not in params:
        params['verbosity'] = 0
        print(f"verbosity set to {params['verbosity']}")


def add_cmdline_params(extra_flags, params):
    for flag in extra_flags:
        keys_str, _, value = flag.rpartition('=')
        keys = keys_str.split('.')
        keys, last = keys[:-1], keys[-1]
        curr = params
        for key in keys:
            try:
                curr = curr[key]
            except Exception:
                raise RuntimeError(f"Keys {keys_str} are not in params.")
        curr[last] = yaml.safe_load(value)


def is_settings_file(filename):
    _, file_ext = os.path.splitext(filename)
    if file_ext in ['.yaml', '.yml']:
        if not os.path.isfile(filename):
            raise FileNotFoundError(f"{filename}: No such script found")
        return True
    else:
        return False


def is_parseable_dict(cmd_line):
    try:
        res = ast.literal_eval(cmd_line)
        return isinstance(res, dict)
    except Exception as e:
        print('WARNING: Dict literal eval suppressed exception: ', e)
        return False


def load_settings(filename):
    with open(filename, 'r') as f:
        settings = yaml.safe_load(f)
        unpack_imports_full(settings, 'import')
        return settings


def set_seed(seed, incr):
    if seed is not None:
        seed += incr
        random.seed(seed)
        torch.manual_seed(seed)
        # torch.cuda.manual_seed(seed)
        numpy.random.seed(seed)


def dfs_on_dicts(orig_dict):
    for key, value in orig_dict.items():
        if isinstance(value, collections.abc.Mapping):
            yield from dfs_on_dicts(value)
    yield orig_dict


def unpack_imports_full(orig_dict, import_string):
    for current_dict in dfs_on_dicts(orig_dict):
        if import_string in current_dict:
            new_file = current_dict.pop(import_string)
            if is_settings_file(new_file):
                new_dict = load_settings(new_file)
                update_recursive(current_dict, new_dict, overwrite=False)


def update_recursive(d, u, overwrite=False):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update_recursive(d.get(k, {}), v, overwrite)
        elif k not in d or overwrite:
            d[k] = v
    return d
