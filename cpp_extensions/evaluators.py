import torch

# # Uncomment for JIT compilation
# from torch.utils.cpp_extension import load
#
# cpp_evaluators = load(name="cpp_evaluators",
#                  sources=["src/main.cpp"],
#                  verbose=True)

from cpp_evaluators import CppPatrollingEvaluator, CppPeriodicMaintenanceEvaluator


class PatrollingProbabilities(torch.nn.Module):
    def __init__(self, init_data, path_merge_threshold=0):
        super().__init__()
        self.path_merge_threshold = path_merge_threshold
        self.evaluator = CppPatrollingEvaluator(**init_data)
        self.evaluator_params = {
            'pmt': path_merge_threshold
        }

    def forward(self, strategy):
        # no kwargs in apply
        # https://github.com/pytorch/pytorch/issues/16940
        return EvaluatorWrapper.apply(strategy, self.evaluator, self.evaluator_params)


class PeriodicMaintenanceRewards(torch.nn.Module):
    def __init__(self, init_data):
        super().__init__()
        self.evaluator = CppPeriodicMaintenanceEvaluator(**init_data)

    def forward(self, strategy, delay, smoothing=0):
        strategy = torch.stack([strategy, delay])
        evaluator_params = {
            'smoothing': smoothing
        }
        return EvaluatorWrapper.apply(strategy, self.evaluator, evaluator_params)


class EvaluatorWrapper(torch.autograd.Function):
    @staticmethod
    def forward(ctx, strategy, evaluator, evaluator_params):
        prob = evaluator.forward(strategy.detach(), **evaluator_params)
        ctx.evaluator = evaluator
        return prob

    @staticmethod
    def backward(ctx, grad):
        strategy_grad = ctx.evaluator.backward(grad)
        return strategy_grad, None, None
