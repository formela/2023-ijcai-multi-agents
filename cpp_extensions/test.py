import torch
from evaluators import PatrollingProbabilities, PeriodicMaintenanceRewards


class Strategy(torch.nn.Module):
    def __init__(self, mask):
        super().__init__()
        self.mask = mask

    def forward(self, strategy_par):
        strategy = torch.exp(strategy_par) * self.mask
        strategy = torch.nn.functional.normalize(strategy, p=1, dim=1)
        return strategy


class Delay(torch.nn.Module):
    def __init__(self, mask):
        super().__init__()
        self.mask = mask

    def forward(self, delay_par):
        delay = torch.exp(delay_par) * self.mask
        return delay


class Test_Patrolling(torch.nn.Module):
    def __init__(self, mask, init_data, path_merge_threshold):
        super().__init__()
        self.get_strategy = Strategy(mask)
        self.get_probabilities = PatrollingProbabilities(init_data, path_merge_threshold)

    def forward(self, strategy_par):
        strategy = self.get_strategy(strategy_par)
        return self.get_probabilities(strategy)


class Test_PeriodicMaintenance(torch.nn.Module):
    def __init__(self, mask, init_data, smoothing):
        super().__init__()
        self.get_strategy = Strategy(mask)
        self.get_delay = Delay(mask)
        self.get_rewards = PeriodicMaintenanceRewards(init_data)
        self.smoothing = smoothing

    def forward(self, strategy_par, delay_par):
        strategy = self.get_strategy(strategy_par)
        delay = self.get_delay(delay_par)
        return self.get_rewards(strategy, delay, self.smoothing)


def get_random_strategy(mask, seed=None):
    if seed is not None:
        torch.manual_seed(seed)

    param = torch.rand_like(mask, dtype=torch.float64) * mask
    strategy = torch.nn.functional.normalize(param, p=1, dim=1)
    return strategy


def get_random_delay(mask, seed=None):
    if seed is not None:
        torch.manual_seed(seed)

    param = torch.rand_like(mask, dtype=torch.float64)
    delay = torch.exp(param) * mask
    return delay


def patrolling():
    data = dict(memory=[1, 1, 2],
                attack_len=[1.0, 2.0],
                value=[3.0, 4.0],
                blindness=[0.0, 0.0],
                edges=[(0, 1, 1.0),
                       (0, 2, 1.0),
                       (1, 2, 1.0),
                       (1, 0, 1.0),
                       (2, 0, 1.0),
                       (2, 1, 1.0)])

    get_probabilities = PatrollingProbabilities(data, path_merge_threshold=0)

    mask = torch.tensor([[0, 1, 1, 1],
                         [1, 0, 1, 1],
                         [1, 1, 0, 0],
                         [1, 1, 0, 0]], dtype=torch.bool)

    strategy = get_random_strategy(mask, 0)
    # Define your own fixed valid strategy, if needed
    # strategy = torch.tensor([[0.0000, 0.3027, 0.1624, 0.5349],
    #                        [0.4412, 0.0000, 0.0500, 0.5088],
    #                        [0.4113, 0.5887, 0.0000, 0.0000],
    #                        [0.3173, 0.6827, 0.0000, 0.0000]], dtype=torch.float64)
    strategy = torch.nn.Parameter(strategy, requires_grad=True)
    print(f'{strategy=}')

    probabilities = get_probabilities(strategy)
    print(f'{probabilities=}')

    steals = torch.tensor(data['value'])[:, None] * probabilities
    print(f'{steals=}')

    value = max(data['value']) - torch.max(steals).item()
    print(f'{value=}')

    loss = torch.sum(torch.flatten(steals) ** 2)
    loss.backward()

    print(f'{strategy.grad=}')

    # Gradient Check
    torch.autograd.gradcheck(Test_Patrolling(mask, data, 2.0),
                             [torch.log(strategy.double())],
                             atol=1e-5, rtol=0, raise_exception=True)


def periodic_maintenance():
    # data = dict(memory=[1, 1],
    #             delimiters=[[1.0, 2.0, 3.0], [1.0]],
    #             rewards=[[1.0, 5.0, 1.0, -10.0], [0.0, 0.0]],
    #             edges=[(0, 1, 1.0),
    #                    (1, 0, 1.0),
    #                    (1, 1, 1.0)])
    data = dict(memory=[1, 1, 1],
                delimiters=[[3, 5], [3, 5], [3, 5]],
                rewards=[[0, 1, -1, 0], [0, 1, -1, 0], [0, 1, -1, 0]],
                edges=[(0, 1, 2),
                       (0, 2, 2),
                       (1, 2, 2),
                       (1, 0, 2),
                       (2, 1, 2),
                       (2, 0, 2)]
                )

    smoothing = 1.0
    get_rewards = PeriodicMaintenanceRewards(data)

    # mask = torch.tensor([[0, 1],
    #                      [1, 1]], dtype=torch.bool)
    mask = torch.tensor([[0, 1, 1],
                         [1, 0, 1],
                         [1, 1, 0]], dtype=torch.bool)

    strategy = get_random_strategy(mask, 0)
    # Define your own fixed valid strategy, if needed
    # strategy = torch.tensor([[0.3, 0.7],
    #                          [0.6, 0.4]], dtype=torch.float64)
    strategy = torch.nn.Parameter(strategy, requires_grad=True)
    print(f'{strategy=}')

    delay = get_random_delay(mask, 0)
    # Define your own delays, if needed
    # delay = torch.tensor([[0.0, 0.0],
    #                       [0.0, 0.0]], dtype=torch.float64)
    delay = torch.nn.Parameter(delay, requires_grad=True)
    print(f'{delay=}')

    rewards = get_rewards(strategy, delay, smoothing)
    print(f'{rewards=}')

    loss = -torch.sum(torch.flatten(rewards))
    loss.backward()

    print(f'{strategy.grad=}')
    print(f'{delay.grad=}')

    # Gradient Check
    torch.autograd.gradcheck(Test_PeriodicMaintenance(mask, data, smoothing),
                             [torch.log(strategy).double(), torch.log(delay).double()],
                             atol=1e-5, rtol=0, raise_exception=True)


if __name__ == '__main__':
    patrolling()
    periodic_maintenance()
