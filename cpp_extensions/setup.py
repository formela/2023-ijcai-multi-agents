from setuptools import setup
from torch.utils.cpp_extension import BuildExtension, CppExtension


setup(name='cpp_evaluators',
      ext_modules=[
            CppExtension(
                  'cpp_evaluators',
                  ['src/main.cpp'],
                  extra_compile_args=['-std=c++17', '-D_SILENCE_CXX17_RESULT_OF_DEPRECATION_WARNING=1']
            ),
      ],
      cmdclass={'build_ext': BuildExtension},
      description="Strategy evaluator")
