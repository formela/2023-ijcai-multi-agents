#include <torch/extension.h>
#include <ATen/ATen.h>

#include "SCCs.h"

using namespace pybind11::literals;

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
    m.doc() = "Plugin for fast strategy evaluation";

    py::class_<SCCs::EVAL>(m, "CppSCCsEvaluator")
        .def(py::init<int>(),
                "Initializes the number of vertices",
            "n"_a)
        .def("get_all_sccs",
            &SCCs::EVAL::PyGetAllSCCs,
            "Returns all strongly connected components as a 2D tensor:\n"
            "The [i][j] entry says whether the j-th vertex lies in the i-th SCC.",
            "strategy"_a)
        .def("get_bottom_sccs",
            &SCCs::EVAL::PyGetBottomSCCs,
            "Returns all bottom strongly connected components as a 2D tensor:\n"
            "The [i][j] entry says whether the j-th vertex lies in the i-th BSCC.",
            "strategy"_a);
}
