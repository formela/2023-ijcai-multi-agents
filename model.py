import numpy as np
import torch
from termcolor import cprint
from cpp_evaluators import CppSCCsEvaluator


class Strategy(torch.nn.Module):
    def __init__(self, graph, rounding_threshold, loader=None, loader_params=None, indep_agents=False):
        super().__init__()
        self.threshold = rounding_threshold
        if indep_agents and graph.num_of_agents > 1:
            self.num_of_agents = graph.num_of_agents
            self.mask = graph.mask_before_ma[None, ...]
            self.mask_size = torch.Size((self.num_of_agents, *graph.mask_before_ma.size()))
        else:
            self.num_of_agents = 1
            self.mask = graph.mask
            self.mask_size = self.mask.size()
        if loader is not None:
            strategy = strategy_loaders[loader](**loader_params)
            print(strategy)
            assert strategy.size() == self.mask_size
            params = self.mask * torch.log(strategy + rounding_threshold / 2)
        else:
            params = self._init_params()
        self.params = torch.nn.Parameter(params)

    def _init_params(self):
        return self.mask * torch.log(torch.rand(size=self.mask_size, dtype=torch.float64))

    def reset_parameters(self):
        self.params.data = self._init_params()

    def schedule(self, total_steps, current_step):
        pass

    def combine_strategies(self, strategy):
        n = self.num_of_agents
        w = strategy[0].shape[0]
        comb_strat = strategy[0].reshape([w] + [1] * (n-1) + [w] + [1] * (n-1))
        for i in range(1, n):
            comb_strat = comb_strat * strategy[i].reshape([1] * i + [w] + [1] * (n-1) + [w] + [1] * (n-1-i))
        return torch.flatten(torch.flatten(comb_strat, 0, self.num_of_agents-1), -self.num_of_agents, -1)

    def forward(self):
        x = torch.exp(self.params) * self.mask
        strategy = torch.nn.functional.normalize(x, p=1, dim=-1)
        if not self.training:
            x = torch.threshold(strategy, threshold=self.threshold, value=0.0)
            strategy = torch.nn.functional.normalize(x, p=1, dim=-1)
        if self.num_of_agents == 1:
            return strategy
        return self.combine_strategies(strategy)


class Model(torch.nn.Module):
    def __init__(self, graph, strategy_params, objective, objective_params=None, delay_params=None, checkpoint=None):
        super().__init__()
        objective_params = objective_params or {}
        self.graph = graph
        self.strategy = Strategy(graph, **strategy_params)
        self.objective = objectives_dict[objective](graph, **objective_params)

        if checkpoint:
            self.load_state_dict(state_dict=torch.load(checkpoint))

    def schedule(self, *args, **kwargs):
        self.strategy.schedule(*args, *kwargs)
        self.objective.schedule(*args, *kwargs)

    def forward(self):
        strategy = self.strategy()
        return self.objective(strategy)


class GenericObjective(object):
    def __init__(self, graph, relaxed_max=None, relaxed_max_params=None, relaxed_min=None, relaxed_min_params=None, verbose=False):
        self.graph = graph
        self.mask = graph.mask
        self.times = graph.times
        self.longest = graph.longest
        self.costs = graph.costs
        self.aug_costs = graph.aug_costs
        self.target_mask = graph.target_mask
        self.non_target_mask = ~graph.target_mask
        self.target_map = graph.aug_target_map
        self.aug_nodes_n = graph.aug_nodes_n
        self.targets_n = graph.targets_n

        self.verbose = verbose
        if relaxed_max:
            relaxed_max_params = relaxed_max_params or {}
            self.relaxed_max = loss_from_string(relaxed_max)(**relaxed_max_params)
        if relaxed_min:
            relaxed_min_params = relaxed_min_params or {}
            self.relaxed_min = loss_from_string(relaxed_min)(**relaxed_min_params)

    def early_stop(self, val):
        return False

    def schedule(self, total_steps, current_step):
        pass

    def get_expectations(self, strategy):
        A = torch.eye(self.aug_nodes_n) - strategy * self.non_target_mask[None, ..., None]
        b = (strategy * self.times).sum(dim=-1) * self.non_target_mask[None, ...]
        return solve_system(A, b, self.verbose)

    def get_variance(self, strategy, expectations):
        A = torch.eye(self.aug_nodes_n) - strategy * self.non_target_mask[None, ..., None]
        b = (strategy * (self.times[None, :] + expectations[..., None, :] - expectations[..., None]) ** 2).sum(dim=-1) * self.non_target_mask[None, ...]
        return solve_system(A, b, self.verbose)

    def get_std(self, strategy, expectations):
        return torch.sqrt(self.get_variance(strategy, expectations) + 1e-14)


class AdversarialPatrolling(GenericObjective, torch.nn.Module):
    mode = 'min'

    def __init__(self, graph, beta=1, rounding_threshold=0.01, incoming_len_softening=0.01, resilience_rate=0,
                 relaxed_max=None, relaxed_max_params=None, relaxed_min=None, relaxed_min_params=None, verbose=False):
        torch.nn.Module.__init__(self)
        GenericObjective.__init__(self, graph=graph, relaxed_max=relaxed_max, relaxed_max_params=relaxed_max_params,
                                  relaxed_min=relaxed_min, relaxed_min_params=relaxed_min_params, verbose=verbose)
        self.beta = beta
        self.bscc = CppSCCsEvaluator(graph.aug_nodes_n)
        self.incoming_len_softening = incoming_len_softening  # for the usage of incoming edges
        self.resilience_rate = resilience_rate  # for residual costs after removing agents
        self.rounding_threshold = rounding_threshold  # for bscc components

    def get_bscc_masks(self, strategy, round_by_threshold=True):
        with torch.no_grad():
            rounded_strategy = strategy.detach()
            if round_by_threshold:
                x = torch.threshold(rounded_strategy, threshold=self.rounding_threshold, value=0.0)
                rounded_strategy = torch.nn.functional.normalize(x, p=1, dim=-1)
            bscc_masks = self.bscc.get_bottom_sccs(rounded_strategy)

            # erase all BSCCs that do not visit all targets
            num_of_visits = torch.sum(bscc_masks[:, None, :] * self.graph.target_mask[None, :, :], dim=-1)  # shape: (BSCCs, Targets+AugTargets)
            to_be_erased = torch.any(num_of_visits == 0, dim=-1)  # shape: (BSCCs)
            return bscc_masks[~to_be_erased]

    def forward(self, strategy):
        bscc_masks = self.get_bscc_masks(strategy, self.training)  # round also in the training mode

        if bscc_masks.size(dim=0) == 0:
            # there is no reasonable BSCC
            cprint('All BSCCs are omitting some of the targets!', 'red')
            print('Possibly due to a very high rounding threshold.')
            if not self.training:
                return float('inf'), strategy, {'bscc_index': -1}
            # try again without rounding
            cprint('The computation continuous without rounding.', 'green')
            bscc_masks = self.get_bscc_masks(strategy, False)
            if bscc_masks.size(dim=0) == 0:
                cprint('Even without rounding, all BSCCs are omitting some of the targets!', 'red')
                self.graph.print_strategy(strategy)
                return None

        bscc_strategies = bscc_masks[:, None, :] * bscc_masks[:, :, None] * strategy  # shape: (BSCCs, Nodes, Nodes)
        bscc_strategies = bscc_strategies[:, None, ...]  # shape: (BSCCs, 1, Nodes, Nodes)

        time_e = self.get_expectations(bscc_strategies)  # shape: (BSCCs, Targets+AugTargets, Nodes)
        time_std = self.get_std(bscc_strategies, time_e)  # shape: (BSCCs, Targets+AugTargets, Nodes)
        cost_std = self.aug_costs[..., None] * time_std  # shape: (BSCCs, Targets+AugTargets, Nodes)

        # code for edges of different length:
        mask_used = (bscc_strategies > 0) if not self.training else torch.pow(bscc_strategies+0.0000001, self.incoming_len_softening)
        time_e = time_e + (self.times * mask_used).max(dim=-2).values  # increment length of the longest used incoming edge
        cost_e = self.aug_costs[..., None] * time_e

        bscc_vals = cost_e + self.beta * cost_std

        bscc_vals_all = torch.reshape(bscc_vals[:, :self.targets_n], (bscc_vals.shape[0], -1))  # shape: (BSCCs, Targets x nodes)
        bscc_maxs_all = bscc_vals_all.max(dim=-1).values

        if self.resilience_rate > 0:
            bscc_vals_residual = torch.reshape(bscc_vals[:, self.targets_n:], (bscc_vals.shape[0], -1))  # shape: (BSCCs, AugTargets x nodes)
            bscc_maxs_residual = bscc_vals_residual.max(dim=-1).values
            val_min, bscc_index = (bscc_maxs_all + self.resilience_rate * bscc_maxs_residual).min(dim=-1)
        else:
            val_min, bscc_index = bscc_maxs_all.min(dim=-1)

        if self.verbose:
            np.set_printoptions(linewidth=200, precision=4, suppress=True)
            print("\nCost std:", cost_std.numpy())
            print("\nVals:\n", bscc_vals.numpy())
            print("\nVal_max in BSCC:", bscc_maxs_all.numpy())
            if self.resilience_rate > 0:
                print("\nResidual val_max in BSCC:", bscc_maxs_residual.numpy())
            print("\nVal min:", val_min.item())
            print("\nOptimal BSCC index:", bscc_index.item())
            print("\nCosts for attack opportunities:", time_e.numpy(), "\n\n")

        extras = {
            'bscc_index': bscc_index.item(),
            'e': time_e[bscc_index, :self.targets_n].detach().numpy().max(),
            'std': time_std[bscc_index, :self.targets_n].detach().numpy().max(),
        }
        if self.resilience_rate > 0:
            extras['resilience'] = bscc_maxs_residual[bscc_index].detach().item()

        if self.training:
            relaxed_maxs_all = self.relaxed_max(bscc_vals_all)
            if self.resilience_rate > 0:
                relaxed_maxs_residual = self.relaxed_max(bscc_vals_residual)
                loss = self.relaxed_min(relaxed_maxs_all + self.resilience_rate * relaxed_maxs_residual)
            else:
                loss = self.relaxed_min(relaxed_maxs_all)

            return loss, val_min.item(), extras

        # return for eval
        strategy_for_optimal_bscc = bscc_strategies[bscc_index].reshape(bscc_strategies.shape[-2], bscc_strategies.shape[-1])
        return val_min.item(), strategy_for_optimal_bscc.detach().numpy(), extras


def print_grad(steal_grads):
    cprint('steal_grads:', 'blue')
    with np.printoptions(precision=3, suppress=True, threshold=np.inf, linewidth=np.inf):
        for grad in steal_grads:
            print(' ', np.array2string(grad.numpy(), prefix='  '), '\n')


def solve_system(A, b, verbose=False):
    try:
        solution = torch.linalg.solve(A, b)
    except RuntimeError as error_msg:
        cprint('torch.linalg.solve failed with runtime error:', 'red')
        print(error_msg)
        if verbose:
            cprint('A:', 'blue')
            print(A)
            cprint('b:', 'blue')
            print(b)
        return None
    return solution


class RoundDown(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x, base):
        if base == 0:
            return x
        else:
            return base * torch.floor(x / base)

    @staticmethod
    def backward(ctx, x_grad):
        return x_grad, None


class GenericMinMax(object):
    def __init__(self, mode):
        if mode not in ['min', 'max']:
            raise ValueError(f'mode {mode} must be on of "min"/"max"')
        self.mode = mode


class HardMinMax(GenericMinMax, torch.nn.Module):
    def __init__(self, mode='max'):
        torch.nn.Module.__init__(self)
        GenericMinMax.__init__(self, mode=mode)

    def forward(self, x):
        if self.mode == 'max':
            x = torch.max(x)
        elif self.mode == 'min':
            x = torch.min(x)
        return x


class PNormClampMinMax(GenericMinMax, torch.nn.Module):
    def __init__(self, eps, ord, dim=-1, mode='max'):
        torch.nn.Module.__init__(self)
        GenericMinMax.__init__(self, mode=mode)
        self.eps = eps
        self.ord = ord
        self.dim = dim

    def forward(self, x):
        if self.mode == 'max':
            max_x = x.max(dim=self.dim, keepdim=True).values
            max_x_static = max_x.detach()
            eps = self.eps * max_x_static
            x = (x - max_x_static + eps) / eps
            x = torch.clamp(x, min=0.0)
            x = (x ** self.ord).sum(dim=self.dim)
        else:
            min_x = x.min(dim=self.dim, keepdim=True).values
            min_x_static = min_x.detach()
            eps = self.eps * min_x_static
            x = (x - min_x_static - eps) / eps
            x = torch.clamp(x, max=0.0)
            x = -((-x) ** self.ord).sum(dim=self.dim)
        return x


class PNormMinMax(GenericMinMax, torch.nn.Module):
    def __init__(self, ord, mode='max'):
        torch.nn.Module.__init__(self)
        GenericMinMax.__init__(self, mode=mode)
        self.ord = ord

    def forward(self, x):
        x = torch.flatten(x)
        x = torch.sum(torch.abs(x ** self.ord))
        return x


objectives_dict = {
    'AdversarialPatrolling': AdversarialPatrolling,
}

loss_dict = {'Hard': HardMinMax,
             'PNorm': PNormMinMax,
             'PNormClamp': PNormClampMinMax,
}


def load_strategy_file(path):
    strategy = np.load(path, allow_pickle=True)
    return torch.as_tensor(strategy, dtype=torch.float64)


def load_strategy_nparray(array):
    return torch.as_tensor(array, dtype=torch.float64)


strategy_loaders = {
    'file': load_strategy_file,
    'array': load_strategy_nparray,
}


def loss_from_string(loss):
    return loss_dict[loss]
